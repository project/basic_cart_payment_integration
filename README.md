# Basic Cart Payment Integration

## About
Integrates basic cart with payment module, it requires Payment Reference Field to module to be enabled.

For more information see:
* [basic cart](https://www.drupal.org/project/basic_cart)
* [payment](https://www.drupal.org/project/payment)

## Getting started
* Enable the module, then go to basic cart settings and click on ** new ** payment tab
* The following setting needs to be checked: Send an email to the customer only after an order is placed and payment is succesfully received.

* Basic Cart: Check the option: BASIC CART ORDER
(Check if you want to create order for the cart.)
* Payment: Have at least one payment method available
* Currency: Have at least one available.

## NOTE
This module is still in development stage, if you find a bug or have a request, get in touch with the maintainers.
Visit modules page

## Roadmap
* Create own payment method plugin, to be used
* Create tests
* Add module to the security audit
* Add more payment subscribers
* Create documentation
* Video demo
* Add logging
