<?php
/**
 * @file
 * Contains \Drupal\basic_cart_payment_integration\Form\BasicCartPaymentIntegration
 */

namespace Drupal\basic_cart_payment_integration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class BasicCartPaymentIntegrationForm
 * @package Drupal\basic_cart_payment_integration\Form
 */
class BasicCartPaymentIntegrationSettingsForm extends ConfigFormBase {

  /**
   * (@inheritDoc)
   */
  public function getFormId() {
    return 'basic_cart_payment_integration_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'basic_cart_payment_integration.settings',
    ];
  }


  /**
   * (@inheritDoc)
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Load basic_cart_configuration.
    $basic_cart_configuration = \Drupal::config('basic_cart.settings');
    $basic_cart_payment_configuration = \Drupal::config('basic_cart_payment_integration.settings');
    $chosen_content_types = array_filter($basic_cart_configuration->get('content_type'));

    // Check if content types are empty.
    if (!empty($chosen_content_types)) {
      $form['basic_cart_payment_integration'] = array(
        '#title' => t('Settings'),
        '#type' => 'fieldset',
        '#description' => t('Settings'),
      );
      // Over ride send email to user.
      $form['basic_cart_payment_integration']['basic_cart_send_emailto_user_override'] = array(
        '#type' => 'checkbox',
        '#title' => $this->t('Send an email to the customer <strong> only after an order is placed and payment is succesfully received.</strong>'),
        '#description' => $this->t('This will override the setting in the <strong>Checkout tab: Send an email to the customer after an order is placed.<strong>'),
        '#default_value' => $basic_cart_payment_configuration->get('basic_cart_send_emailto_user_override'),
      );
    }
    else {
      $basic_cart_url = Url::fromRoute('basic_cart.settings');
      $basic_cart_tab = \Drupal::l(t('Basic Cart settings tab'), $basic_cart_url);
      $description = t('Go to');
      $description .= ' ' . $basic_cart_tab . ' ';
      $description .= t('and select the content
types for which you wish to have the "Add to cart" option. <br /> Come back here after.');

      $form['basic_cart_payment_integration'] = array(
        '#title' => t('Content types '),
        '#type' => 'fieldset',
        '#description' => $description,
      );
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * (@inheritDoc)
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $overide_notification = $form_state->getValue('basic_cart_send_emailto_user_override');
    $this->config('basic_cart_payment_integration.settings')
      ->set('basic_cart_send_emailto_user_override', $overide_notification)
      ->save();
    // Set off notification from basic_cart.
    $config_basic_cart_checkout = \Drupal::getContainer()
      ->get('config.factory')
      ->getEditable('basic_cart.checkout');
    if ($overide_notification) {
      $config_basic_cart_checkout->set('send_emailto_user', 0)
        ->save();
    }
    // Re-direct user to make-payment page.
    $basic_cart_thank_you =  $config_basic_cart_checkout->get('thankyou');

    $this->config('basic_cart_payment_integration.settings')
      ->set('basic_cart_thankyou_override', $basic_cart_thank_you)
      ->save();
    $basic_cart_thank_you['custom_page'] = 'make-payment';
    $config_basic_cart_checkout->set('thankyou', $basic_cart_thank_you)
      ->save();
  }

}
