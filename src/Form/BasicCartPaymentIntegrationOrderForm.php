<?php
/**
 * @file
 * Contains \Drupal\basic_cart_payment_integration\Form\BasicCartPaymentIntegration
 */

namespace Drupal\basic_cart_payment_integration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\basic_cart\Utility;

/**
 * Class BasicCartPaymentIntegrationForm
 * @package Drupal\basic_cart_payment_integration\Form
 */
class BasicCartPaymentIntegrationOrderForm extends ConfigFormBase {

  /**
   * (@inheritDoc)
   */
  public function getFormId() {
    return 'basic_cart_payment_integration_order_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'basic_cart_payment_integration_order_form',
    ];
  }

  /**
   * (@inheritDoc)
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $basic_cart_payment_configuration = \Drupal::config('basic_cart_payment_integration.settings');
    $basic_cart_data = $basic_cart_payment_configuration->get('cart_data');
    $current_user_id = (int) \Drupal::currentUser()->id();
    $payment_config = \Drupal::config('payment_reference.payment_type');

    if (!empty($basic_cart_data)) {
      $line_item_manager = \Drupal::service('plugin.manager.payment.line_item');
      $payment = \Drupal::entityTypeManager()
        ->getStorage('payment')
        ->create(['bundle' => 'payment_reference']);

      foreach ($basic_cart_data as $cart_index => $basic_cart_item) {
        $amount = explode(",", $basic_cart_item["price_value"]);
        $node_storage = \Drupal::entityTypeManager()->getStorage('node');
        $entity = $node_storage->load($cart_index);
        $cart_config = [
          'quantity' => (int) $basic_cart_item["quantity"],
          'currency_code' => 'EUR',
          'amount' => $amount[0],
          'description' => $entity->get('title')->value,
          'name' => $cart_index,
        ];
        $line_item[] = $line_item_manager->createInstance('payment_basic', $cart_config);
      }
      $payment->setLineItems($line_item)->setCurrencyCode('EUR');

      $form['actions']['pay'] = [
        '#queue_category_id' => $entity->getEntityTypeId() . '.' . $entity->Bundle(),
        '#queue_owner_id' => $current_user_id,
        '#plugin_selector_id' => $payment_config->get('plugin_selector_id'),
        '#prototype_payment' => $payment,
        '#type' => 'payment_reference',
        '#weight' => 16,
      ];
    }

    return $form;
  }

  /**
   * (@inheritDoc)
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = Utility::checkoutSettings();
    $location = trim($config->get('thankyou')['custom_page']);
    if ($location) {
      $redirect = \Drupal::pathValidator()
        ->getUrlIfValid($location);
      $form_state->setRedirectUrl($redirect);
    }
    else {
      $url = Url::fromRoute('basic_cart.thankyou');
      $form_state->setRedirectUrl($url);
    }
  }

}
