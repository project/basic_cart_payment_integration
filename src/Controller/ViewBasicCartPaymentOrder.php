<?php

/**
 * @file
 * Contatins \Drupal\basic_cart_payment_integration\ViewBasicCartPaymentOrder
 */

namespace Drupal\basic_cart_payment_integration\Controller;

use Drupal\Core\Controller\ControllerBase;
/**
 * Contains the cart controller.
 */
class ViewBasicCartPaymentOrder extends ControllerBase {

  public function payment() {
    $basic_cart_payment_configuration = \Drupal::config('basic_cart_payment_integration.settings');
    $basic_cart_data = $basic_cart_payment_configuration->get('cart_data');
    if (empty($basic_cart_data)) {
      return [
        '#type' => 'markup',
        '#markup' => t('Your cart is empty'),
      ];
    }
    else {
      $form_class = '\Drupal\basic_cart_payment_integration\Form\BasicCartPaymentIntegrationOrderForm';
      $node_create_form = \Drupal::formBuilder()->getForm($form_class);
      return array(
        '#type' => 'markup',
        '#markup' => render($node_create_form),
      );
    }
  }

}
