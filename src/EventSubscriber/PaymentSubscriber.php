<?php

/**
 * @file
 * Contains Drupal\basic_cart_payment_integration\EventSubscriber.
 */

namespace Drupal\basic_cart_payment_integration\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\payment\Event\PaymentStatusSet;
use Drupal\payment\Event\PaymentEvents;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Url;

/**
 * Class OnPaymentSubscriber.
 *
 * @package Drupal\module
 */
class PaymentSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    $events[PaymentEvents::PAYMENT_STATUS_SET] = [
      'redirectToSpecificPage',
      100
    ];
    return $events;
  }

  /**
   * @param PaymentStatusSet $event
   */
  public function redirectToSpecificPage(PaymentStatusSet $event) {
    $payment = $event->getPayment();
    $payment_status = $payment->getPaymentStatus();
    if ($payment_status->getBaseId() == 'payment_pending') {
      $url = Url::fromRoute('basic_cart.thankyou')->toString();
      $response = new RedirectResponse($url);
      $response->send();
    }
  }
}
